#include "Analysis.h"
#include "TMath.h"

#include <iostream>

int main(int argc, char* argv[]) {
    if(argc < 2){
        std::cout << "no config file passed";
        exit(-1);
    }
    Analysis a;
    a.Parse_config(argv[1]);
    a.Parse_dataset();
    Double_t edges[4] = {0.0, 2.5, 2.85, TMath::Pi()};
    TH1D *hist = new TH1D("","", 3, edges);
    hist->GetXaxis()->SetTitle("deltaPhi_ph_MET");
    hist->GetYaxis()->SetTitle("Entries");
    a.Var_analysis(hist, "deltaPhi_ph_MET");

    return 0;
}
