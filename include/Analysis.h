#ifndef SLICE_METHOD_ANALYSIS_H
#define SLICE_METHOD_ANALYSIS_H

#include "Rtypes.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TH1.h"

#include <string>
#include <vector>
#include <map>

class Analysis{
public:
    Analysis();
    ~Analysis(){};
    void Parse_config(std::string config);
    void Parse_dataset();
    void Var_analysis(TH1D *hist, TString var_name);
    void Check();
private:
    void Get_file_content(TString filename);
    void Get_koef_set(TString filename);
    void Count_extra_values();

    void Make_template_histos(TH1D *hist, TString var_name, std::vector<Bool_t(Analysis::*)()> selections, Int_t CR_num);
    void Make_data_hist(TH1D *hist, TString var_name, std::vector<Bool_t(Analysis::*)()> selections, Bool_t(Analysis::*signif_selection)());
    void Make_signal_hist(TH1D *hist, TString var_name, std::vector<Bool_t(Analysis::*)()> selections, Bool_t(Analysis::*signif_selection)());
    void Make_other_bg_hist(TH1D *hist, TString var_name, std::vector<Bool_t(Analysis::*)()> selections, Bool_t(Analysis::*signif_selection)());

    std::map<TString, std::vector<TString>> bg_files;
    std::map<TString, Int_t> bg_colors;

    std::vector<Bool_t(Analysis::*)()> SR_selections;
    std::vector<Bool_t(Analysis::*)()> FR_selections;
    std::vector<Bool_t(Analysis::*)()> slice_selections;

    Bool_t jet_selection();
    Bool_t deltaPhi_ph_MET_selection();
    Bool_t metTST_pt_selection();
    Bool_t ph_pt_selection();
    Bool_t n_ph_selection();
    Bool_t ph_isem_selection();
    Bool_t calo_iso_selection();
    Bool_t track_iso_selection();
    Bool_t ph_z_point_selection();
    Bool_t lepton_veto_selection();
    Bool_t weight_selection();
    Bool_t metTSTsignif_selection();

    Bool_t default_true(){return true;};

    Bool_t jet_selection_reversed();
    Bool_t deltaPhi_ph_MET_selection_reversed();
    Bool_t metTST_pt_selection_reversed();
    Bool_t ph_pt_selection_reversed();
    Bool_t n_ph_selection_reversed();
    Bool_t ph_isem_selection_reversed();
    Bool_t calo_iso_selection_reversed();
    Bool_t track_iso_selection_reversed();
    Bool_t ph_z_point_selection_reversed();
    Bool_t lepton_veto_selection_reversed();
    Bool_t weight_selection_reversed();
    Bool_t metTSTsignif_selection_reversed();

    Bool_t slice1_metTSTsignif_selection();
    Bool_t slice2_metTSTsignif_selection();
    Bool_t slice3_metTSTsignif_selection();
    Bool_t slice4_metTSTsignif_selection();
    Bool_t slice5_metTSTsignif_selection();

    Double_t weight, deltaPhi_jet_lead_MET, deltaPhi_ph_MET, metTSTsignif, metTST_pt, ph_z_point, metTST_phi,
    ph_pt, ph_eta, ph_phi, ph_iso_et20 , ph_iso_pt, jet_lead_E, jet_lead_eta, jet_lead_phi,
    jet_lead_pt,  soft_term_pt, calo_iso, track_iso;
    Double_t koef_set;
    UInt_t n_ph, n_mu, n_e_looseBL, n_jet, ph_isem, n_entries_tr;
    std::string dataset_path;
    TFile *file_ptr;
    TTree *tr;
    TString file_path;
    TFile *slice_method_output_file;
};

#endif //SLICE_METHOD_ANALYSIS_H
