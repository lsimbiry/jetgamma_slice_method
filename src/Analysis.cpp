#include "Analysis.h"
#include "TLorentzVector.h"
#include "TString.h"
#include "TH1.h"

#include <iostream>
#include <cmath>

Analysis::Analysis(){
    slice_selections.push_back(&Analysis::slice1_metTSTsignif_selection);
    slice_selections.push_back(&Analysis::slice2_metTSTsignif_selection);
    slice_selections.push_back(&Analysis::slice3_metTSTsignif_selection);
    slice_selections.push_back(&Analysis::slice4_metTSTsignif_selection);
    slice_selections.push_back(&Analysis::slice5_metTSTsignif_selection);

    slice_method_output_file = new TFile("slice_method_output_file.root", "RECREATE");
}


void Analysis::Get_file_content(TString filename) {
    file_ptr = new TFile(filename);
    file_path = filename;
    if (!file_ptr || file_ptr->IsZombie()) {
        std::cerr << "Error opening file" << std::endl;
        exit(-1);
    }
    tr = (TTree*)file_ptr->Get("output_tree");
    n_entries_tr = tr->GetEntries();
    tr->SetBranchAddress("weight", &weight);
    tr->SetBranchAddress("metTSTsignif", &metTSTsignif);
    tr->SetBranchAddress("metTST_pt", &metTST_pt);
    tr->SetBranchAddress("metTST_phi", &metTST_phi);
    tr->SetBranchAddress("ph_z_point", &ph_z_point);
    tr->SetBranchAddress("ph_pt", &ph_pt);
    tr->SetBranchAddress("ph_eta", &ph_eta);
    tr->SetBranchAddress("ph_phi", &ph_phi);
    tr->SetBranchAddress("ph_isem", &ph_isem);
    tr->SetBranchAddress("jet_lead_E", &jet_lead_E);
    tr->SetBranchAddress("jet_lead_phi", &jet_lead_phi);
    tr->SetBranchAddress("jet_lead_eta", &jet_lead_eta);
    tr->SetBranchAddress("jet_lead_pt", &jet_lead_pt);
    tr->SetBranchAddress("ph_iso_et20", &ph_iso_et20);
    tr->SetBranchAddress("ph_iso_pt", &ph_iso_pt);
    tr->SetBranchAddress("n_ph", &n_ph);
    tr->SetBranchAddress("n_e_looseBL", &n_e_looseBL);
    tr->SetBranchAddress("n_mu", &n_mu);
    tr->SetBranchAddress("n_jet", &n_jet);
    tr->SetBranchAddress("soft_term_pt", &soft_term_pt);
    Get_koef_set(filename);
}

void Analysis::Get_koef_set(TString filename) {
    Double_t lumi_for_calc_a = 36214.96; // 15-16 годы
    Double_t lumi_for_calc_d = 44307.4; // 17 год
    Double_t lumi_for_calc_e = 58450.1; // 18 год
    Double_t norm_koef;
    Int_t n_entries_tr_sw;
    Double_t sum_of_weights_bk_xAOD, sum(0);
    if (!filename.Contains("data")){
        TTree *tr_sw = (TTree*)file_ptr->Get("output_tree_sw");
        TTree *tr_norm = (TTree*)file_ptr->Get("norm_tree");
        n_entries_tr_sw = tr_sw->GetEntries();
        tr_sw->SetBranchAddress("sum_of_weights_bk_xAOD",&sum_of_weights_bk_xAOD);
        tr_norm->SetBranchAddress("koef", &norm_koef);
        tr_norm->GetEntry(0); // Получение значения для числителя
        for(Int_t i = 0; i < n_entries_tr_sw; i++){
            tr_sw->GetEntry(i);
            sum += sum_of_weights_bk_xAOD; // Подсчёт суммы весов для знаменателя
        }
        if (filename.Contains("MC16a")){
            koef_set = norm_koef * lumi_for_calc_a / sum;
        } else if (filename.Contains("MC16d")) {
            koef_set = norm_koef * lumi_for_calc_d / sum;
        } else if (filename.Contains("MC16e")){
            koef_set = norm_koef * lumi_for_calc_e / sum;
        } else {
            std::cerr << "unknown MC" << std::endl;
            exit(-1);
        }
        delete tr_sw;
        delete tr_norm;
    }else{
        koef_set = 1;
    }
}

void Analysis::Count_extra_values() {
    TLorentzVector met4vec, ph4vec, jet_lead4vec, jet_sublead4vec;
    met4vec.SetPtEtaPhiE(metTST_pt, 0, metTST_phi, metTST_pt);
    ph4vec.SetPtEtaPhiM(ph_pt, ph_eta, ph_phi, 0);
    deltaPhi_ph_MET = fabs(ph4vec.DeltaPhi(met4vec));
    deltaPhi_jet_lead_MET = 10;
    if(n_jet > 0){
        jet_lead4vec.SetPtEtaPhiE(jet_lead_pt, jet_lead_eta, jet_lead_phi, jet_lead_E);
        deltaPhi_jet_lead_MET = fabs(met4vec.DeltaPhi(jet_lead4vec));
    }
    calo_iso = ph_iso_et20 / ph_pt;
    track_iso = ph_iso_pt / ph_pt;
}

void Analysis::Check(){
    Double_t edges[4] = {0.0, 15.0, 22.0, 50.0};
    TH1D *hist = new TH1D("metTSTsignif_data_FR","metTSTsignif_data_FR", 3, edges);
    Make_data_hist(hist, "metTSTsignif", FR_selections, &Analysis::default_true);
    slice_method_output_file->WriteObject(hist, "metTSTsignif_data_FR");
}
