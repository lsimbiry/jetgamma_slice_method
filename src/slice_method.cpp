#include "Analysis.h"
#include "TSystem.h"
#include <iostream>


void Analysis::Var_analysis(TH1D *hist, TString var_name){
    gSystem->mkdir(var_name);
    gSystem->cd(var_name);
    gSystem->mkdir("data-driven");
    gSystem->cd("data-driven");

    TFile *data_file = new TFile("data.root", "RECREATE");
    TFile *signal_file = new TFile("signal.root", "RECREATE");
    TFile *other_bg_file = new TFile("other_bg.root", "RECREATE");

    TString name = (TString)"data_" + var_name + (TString)"_FR_hist";
    TH1D *data_FR_hist = (TH1D*)hist->Clone();
    data_FR_hist->SetNameTitle(name, name);
    std::cout << name << std::endl;
    Make_data_hist(data_FR_hist, var_name, FR_selections, &Analysis::metTSTsignif_selection);
    data_file->WriteObject(data_FR_hist, var_name);

    name = (TString)"signal_" + var_name + (TString)"_FR_hist";
    TH1D *signal_FR_hist = (TH1D*)hist->Clone();
    signal_FR_hist->SetNameTitle(name, name);
    std::cout << name << std::endl;
    Make_signal_hist(signal_FR_hist, var_name, FR_selections, &Analysis::metTSTsignif_selection);
    signal_file->WriteObject(signal_FR_hist, var_name);

    name = (TString)"other_bg_" + var_name + (TString)"_FR_hist";
    TH1D *other_bg_FR_hist = (TH1D*)hist->Clone();
    other_bg_FR_hist->SetNameTitle(name, name);
    Make_other_bg_hist(other_bg_FR_hist, var_name, FR_selections, &Analysis::metTSTsignif_selection);
    std::cout << name << std::endl;
    other_bg_file->WriteObject(other_bg_FR_hist, var_name);

    data_file->Close();
    signal_file->Close();
    other_bg_file->Close();

    Make_template_histos(hist, var_name, FR_selections, 1);
    Make_template_histos(hist, var_name, SR_selections, 2);

}

void Analysis::Make_data_hist(TH1D *hist, TString var_name, std::vector<Bool_t(Analysis::*)()> selections, Bool_t(Analysis::*signif_selection)()){
    Double_t *var_ptr;
    if(var_name == "metTSTsignif"){
        var_ptr = &metTSTsignif;
    } else if (var_name == "deltaPhi_ph_MET"){
        var_ptr = &deltaPhi_ph_MET;
    } else if (var_name == "metTST_pt"){
        var_ptr = &metTST_pt;
    } else {
        std::cout << "unknown var for template" << std::endl;
        exit(-1);
    }

    for (auto& file : bg_files["data"]){
        Get_file_content(file);
        for(Int_t i = 0; i < n_entries_tr; i++){
            tr->GetEntry(i);
            Count_extra_values();
            Bool_t passed = true;
            for (auto& selection : selections){
                passed = passed * (this->*selection)();
                if (!passed){
                    break;
                }
            }
            if (passed && (this->*signif_selection)()){
                hist->Fill(*var_ptr, weight * koef_set);
            }
        }
        delete tr;
        delete file_ptr;
    }
}

void Analysis::Make_other_bg_hist(TH1D *hist, TString var_name, std::vector<Bool_t(Analysis::*)()> selections, Bool_t(Analysis::*signif_selection)()){
    Double_t *var_ptr;
    if (var_name == "metTSTsignif"){
        var_ptr = &metTSTsignif;
    } else if (var_name == "deltaPhi_ph_MET"){
        var_ptr = &deltaPhi_ph_MET;
    } else if (var_name == "metTST_pt"){
        var_ptr = &metTST_pt;
    } else {
        std::cout << "unknown var for template" << std::endl;
        exit(-1);
    }

    for(auto& [type, files] : bg_files){
        if ((type == "jet_to_MET") || (type == "data") || (type == "signal")){
            continue;
        }
        for (auto& file : files){
            Get_file_content(file);
            for(Int_t i = 0; i < n_entries_tr; i++){
                tr->GetEntry(i);
                Count_extra_values();
                Bool_t passed = true;
                for (auto& selection : selections){
                    passed = passed * (this->*selection)();
                    if (!passed){
                        break;
                    }
                }
                if (passed && (this->*signif_selection)()){
                    hist->Fill(*var_ptr, weight * koef_set);
                }
            }
            delete tr;
            delete file_ptr;
        }
    }
}

void Analysis::Make_signal_hist(TH1D *hist, TString var_name, std::vector<Bool_t(Analysis::*)()> selections, Bool_t(Analysis::*signif_selection)()){
    Double_t *var_ptr;
    if(var_name == "metTSTsignif"){
        var_ptr = &metTSTsignif;
    } else if (var_name == "deltaPhi_ph_MET"){
        var_ptr = &deltaPhi_ph_MET;
    } else if (var_name == "metTST_pt"){
        var_ptr = &metTST_pt;
    } else {
        std::cout << "unknown var for template" << std::endl;
        exit(-1);
    }

    for (auto& file : bg_files["signal"]){
        Get_file_content(file);
        for(Int_t i = 0; i < n_entries_tr; i++){
            tr->GetEntry(i);
            Count_extra_values();
            Bool_t passed = true;
            for (auto& selection : selections){
                passed = passed * (this->*selection)();
                if (!passed){
                    break;
                }
            }
            if (passed && (this->*signif_selection)()){
                hist->Fill(*var_ptr, weight * koef_set);
            }
        }
        delete tr;
        delete file_ptr;
    }
}

void Analysis::Make_template_histos(TH1D *hist, TString var_name, std::vector<Bool_t(Analysis::*)()> selections, Int_t CR_num) {
    std::vector<TH1D*> template_slice_histos;
    for (Int_t i = 0; i < 5; i++){
        TString name = (TString)"template_" + var_name + (TString)"_CR" + (TString)std::to_string(CR_num) + "_slice" + (TString)std::to_string(i + 1) + (TString)"_hist";
        std::cout << name << std::endl;
        template_slice_histos.push_back((TH1D*)hist->Clone());
        template_slice_histos[i]->SetNameTitle(name, name);
    }

    Double_t *var_ptr;
    if (var_name == "metTSTsignif"){
        var_ptr = &metTSTsignif;
    } else if (var_name == "deltaPhi_ph_MET"){
        var_ptr = &deltaPhi_ph_MET;
    } else if (var_name == "metTST_pt"){
        var_ptr = &metTST_pt;
    } else {
        std::cout << "unknown var for template" << std::endl;
        exit(-1);
    }

    for(auto& [type, files] : bg_files){
        Double_t sign;
        if (type == "data"){
            sign = 1.0;
        } else {
            sign = -1.0;
        }
        if (type == "jet_to_MET"){
            continue;
        }
        for (auto& file : files){
            Get_file_content(file);
            for(Int_t i = 0; i < n_entries_tr; i++){
                tr->GetEntry(i);
                Count_extra_values();
                Bool_t passed = true;
                for (auto& selection : selections){
                    passed = passed * (this->*selection)();
                    if (!passed){
                        break;
                    }
                }

                if (passed){
                    for(Int_t j = 0; j < 5; j++){
                        if((this->*slice_selections[j])()){
                            template_slice_histos[j]->Fill(*var_ptr, sign * weight * koef_set);
                            break;
                        }
                    }
                }

            }
            delete tr;
            delete file_ptr;
        }
    }

    for (Int_t i = 0; i < 5; i++){
       TString dir_name = "slice" + (TString)std::to_string(i+1);
       gSystem->mkdir(dir_name);
       gSystem->cd(dir_name);
       TString template_file_name = "templateCR" + (TString)std::to_string(CR_num) + ".root";
       TFile *template_file = new TFile(template_file_name, "RECREATE");
       template_file->WriteObject(template_slice_histos[i], var_name);
       template_file->Close();
       gSystem->cd("..");
    }
}