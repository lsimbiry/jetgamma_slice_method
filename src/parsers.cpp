#include "Analysis.h"
#include <fstream>
#include <iostream>
#include <utility>

void Analysis::Parse_config(std::string config){
    std::ifstream conf(config);
    if(!conf){
        exit(-1);
    }
    std::string param, val;
    std::string line;
    while (!conf.eof()){
        getline(conf, line);
        if(conf.eof()){
            break;
        }
        if(line.empty()){
            continue;
        }

        auto pos = line.find(' '); // смотрим на то 1 или более вхождений в строке

        if (pos != std::string::npos){
            param = line.substr(0, pos - 1);
            pos++;
            while(line[pos] == ' '){
                pos++;
            }
            val = line.substr(pos, line.size() - pos);
            if(param == "dataset"){
                dataset_path = val;
            }else {
                std::cout << "unknown command in config: " << param << std::endl;
            }
            std::cout << param << " " << val << std::endl;

        } else {
            std::cout << line << std::endl;
            std::vector<Bool_t(Analysis::*)()> selections;
            std::string line2;
            getline(conf, line2);
            while(!line2.empty()){
                pos = line2.find(' ');
                param = line2.substr(0, pos - 1);
                pos++;
                while(line2[pos] == ' '){
                    pos++;
                }
                val = line2.substr(pos, line2.size() - pos);
                if (param == "jet_selection"){
                    if (val == "ON"){
                        selections.push_back(&Analysis::jet_selection);
                    } else if (val == "REVERSE"){
                        selections.push_back(&Analysis::jet_selection_reversed);
                    }

                } else if (param == "deltaPhi_ph_MET_selection"){
                    if (val == "ON"){
                        selections.push_back(&Analysis::deltaPhi_ph_MET_selection);
                    } else if (val == "REVERSE"){
                        selections.push_back(&Analysis::deltaPhi_ph_MET_selection_reversed);
                    }
                } else if (param == "metTST_pt_selection"){
                    if (val == "ON"){
                        selections.push_back(&Analysis::metTST_pt_selection);
                    } else if (val == "REVERSE"){
                        selections.push_back(&Analysis::metTST_pt_selection_reversed);
                    }
                } else if (param == "ph_pt_selection"){
                    if (val == "ON"){
                        selections.push_back(&Analysis::ph_pt_selection);
                    } else if (val == "REVERSE"){
                        selections.push_back(&Analysis::ph_pt_selection_reversed);
                    }
                } else if (param == "n_ph_selection"){
                    if (val == "ON"){
                        selections.push_back(&Analysis::n_ph_selection);
                    } else if (val == "REVERSE"){
                        selections.push_back(&Analysis::n_ph_selection_reversed);
                    }
                } else if (param == "ph_isem_selection"){
                    if (val == "ON"){
                        selections.push_back(&Analysis::ph_isem_selection);
                    } else if (val == "REVERSE"){
                        selections.push_back(&Analysis::ph_isem_selection_reversed);
                    }
                } else if (param == "calo_iso_selection"){
                    if (val == "ON"){
                        selections.push_back(&Analysis::calo_iso_selection);
                    } else if (val == "REVERSE"){
                        selections.push_back(&Analysis::calo_iso_selection_reversed);
                    }
                } else if (param == "track_iso_selection"){
                    if (val == "ON"){
                        selections.push_back(&Analysis::track_iso_selection);
                    } else if (val == "REVERSE"){
                        selections.push_back(&Analysis::track_iso_selection_reversed);
                    }
                } else if (param == "ph_z_point_selection"){
                    if (val == "ON"){
                        selections.push_back(&Analysis::ph_z_point_selection);
                    } else if (val == "REVERSE"){
                        selections.push_back(&Analysis::ph_z_point_selection_reversed);
                    }
                } else if (param == "lepton_veto_selection"){
                    if (val == "ON"){
                        selections.push_back(&Analysis::lepton_veto_selection);
                    } else if (val == "REVERSE"){
                        selections.push_back(&Analysis::lepton_veto_selection_reversed);
                    }
                } else if (param == "weight_selection"){
                    if (val == "ON"){
                        selections.push_back(&Analysis::weight_selection);
                    } else if (val == "REVERSE"){
                        selections.push_back(&Analysis::weight_selection_reversed);
                    }
                } else if (param == "metTSTsignif_selection"){
                    if (val == "ON"){
                        selections.push_back(&Analysis::metTSTsignif_selection);
                    } else if (val == "REVERSE"){
                        selections.push_back(&Analysis::metTSTsignif_selection_reversed);
                    }
                } else {
                    std::cout << "unknown command in config: " << param << std::endl;
                }

                std::cout << param << " " << val << std::endl;
                getline(conf, line2);
            }
            if (line == "SR_selections"){
                SR_selections = std::move(selections);
            } else if (line == "FR_selections"){
                FR_selections = std::move(selections);
            } else{
                std::cout << "unknown command in config: " << line << std::endl;
            }
        }
        std::cout << std::endl;
    }
}

void Analysis::Parse_dataset(){
    if (dataset_path.empty()){
        std::cout << "define dataset" << std::endl;
        exit(-1);
    }

    std::ifstream dataset(dataset_path);
    TString bg_type;
    Int_t color_id;
    if (!dataset){
        std::cerr << "dataset file not found" << std::endl;
        exit(-1);
    }
    dataset >> bg_type >> color_id;
    while(!dataset.eof()){
        bg_colors[bg_type] = color_id;
        dataset >> file_path;
        while(file_path != "===" && !dataset.eof()){
            bg_files[bg_type].push_back(file_path);
           // std::cout << bg_type << " " <<  file_path << " added" << std::endl;
            dataset >> file_path;
        }
        dataset >> bg_type >> color_id;
    }
}
