#include "Analysis.h"
#include <cmath>

//SELECTONS

Bool_t Analysis::jet_selection(){
    return n_jet == 0 || (n_jet > 0 && deltaPhi_jet_lead_MET > 0.4);
}

Bool_t Analysis::deltaPhi_ph_MET_selection(){
    return deltaPhi_ph_MET > 0.7;
}

Bool_t Analysis::metTST_pt_selection(){
    return metTST_pt > 130;
}

Bool_t Analysis::ph_pt_selection(){
    return ph_pt > 150;
}

Bool_t Analysis::n_ph_selection(){
    return n_ph == 1;
}

Bool_t Analysis::ph_isem_selection(){
    return ph_isem == 0;
}

Bool_t Analysis::calo_iso_selection(){
    return calo_iso < 0.065;
}

Bool_t Analysis::track_iso_selection(){
    return track_iso < 0.05;
}

Bool_t Analysis::ph_z_point_selection(){
    return fabs(ph_z_point) < 250;
}

Bool_t Analysis::lepton_veto_selection(){
    return (n_e_looseBL == 0) && (n_mu == 0);
}

Bool_t Analysis::weight_selection(){
    if(!file_path.Contains("410470")){
        return fabs(weight) < 100;
    } else{
        return true;
    }
}

Bool_t Analysis::metTSTsignif_selection(){
    return metTSTsignif > 11;
}

// REVERSED SELECTIONS

Bool_t Analysis::jet_selection_reversed(){
    return !jet_selection();
}

Bool_t Analysis::deltaPhi_ph_MET_selection_reversed(){
    return !deltaPhi_ph_MET_selection();
}

Bool_t Analysis::metTST_pt_selection_reversed(){
    return !metTST_pt_selection();
}

Bool_t Analysis::ph_pt_selection_reversed(){
    return !ph_pt_selection();
}

Bool_t Analysis::n_ph_selection_reversed(){
    return !n_ph_selection();
}

Bool_t Analysis::ph_isem_selection_reversed(){
    return !ph_isem_selection();
}

Bool_t Analysis::calo_iso_selection_reversed(){
    return !calo_iso_selection();
}

Bool_t Analysis::track_iso_selection_reversed(){
    return !track_iso_selection();
}

Bool_t Analysis::ph_z_point_selection_reversed(){
    return !ph_z_point_selection();
}

Bool_t Analysis::lepton_veto_selection_reversed(){
    return !lepton_veto_selection();
}

Bool_t Analysis::weight_selection_reversed(){
    return !weight_selection();
}

Bool_t Analysis::metTSTsignif_selection_reversed(){
    return !metTSTsignif_selection();
}

// SLICES

Bool_t Analysis::slice1_metTSTsignif_selection(){
    return (metTSTsignif <= 11) && (metTSTsignif > 9);
}

Bool_t Analysis::slice2_metTSTsignif_selection(){
    return (metTSTsignif <= 9) && (metTSTsignif > 7);
}

Bool_t Analysis::slice3_metTSTsignif_selection(){
    return (metTSTsignif <= 7) && (metTSTsignif > 5);
}

Bool_t Analysis::slice4_metTSTsignif_selection(){
    return (metTSTsignif <= 5) && (metTSTsignif > 3);
}

Bool_t Analysis::slice5_metTSTsignif_selection(){
    return metTSTsignif <= 3;
}